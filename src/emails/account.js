const sgMail = require('@sendgrid/mail')

sgMail.setApiKey('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')

const sendWelcomeEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'ayesha.santos@phitopolis.com',
        subject: 'Thanks for joining in!',
        text: `Welcome to the app, ${name}. Let me know how you get along with the app.`
    }).then((response) => {
        console.log(response[0].statusCode)
        console.log(response[0].headers)
    }).catch((error) => {
        console.error(error)
    })
}

const sendCancelationEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'ayesha.santos@phitopolis.com',
        subject: 'Sorry to see you go!',
        text: `Goodbye, ${name}. I hope to see you back sometime soon.`
    }).then((response) => {
        console.log(response[0].statusCode)
        console.log(response[0].headers)
    }).catch((error) => {
        console.error(error)
    })
}

module.exports = {
    sendWelcomeEmail,
    sendCancelationEmail
}