# Task App
## Setup MongoDB
- sudo apt install mongodb
- service mongodb start
## Connect to MongoDB
- used the MongoDB extension available in VSCode for office computer setup
- to access logs, open /var/log/mongodb
## Install all dependencies
- npm install
## Run the application
- node src/index.js
## Setup Postman
https://www.postman.com/downloads/
- launch the .exe file
- create new collection named task-app
- add new request to trigger endpoints

## Add Environment in Postman
- create new environment named task-app
- add url = localhost:3000
- set this environment to active
## Create User
- POST {{url}}/users
{
    "name": "AK Santos",
    "email": "aksantos@gmail.com",
    "password": "12345678",
    "age": 21
}
- Test
if(pm.response.code === 201){
    pm.environment.set('authToken', pm.response.json().token)
}
- Response Body
{
    "user": {
        "name": "AK Santos",
        "email": "aksantos@gmail.com",
        "age": 21,
        "_id": "63ec4eab3cbd3c505734653d",
        "__v": 1
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2M2VjNGVhYjNjYmQzYzUwNTczNDY1M2QiLCJpYXQiOjE2NzY0MzEwMTl9.ORci4x_XODzA81dqvv72N9mA6-RYFYZf4yl8aAhz1eE"
}
## Login User
- POST {{url}}/users/login
{
    "email": "aksantos@gmail.com",
    "password": "12345678"
}
- Test
if(pm.response.code === 200){
    pm.environment.set('authToken', pm.response.json().token)
}
- Response Body
{
    "user": {
        "_id": "63ec4eab3cbd3c505734653d",
        "name": "AK Santos",
        "email": "aksantos@gmail.com",
        "age": 21,
        "__v": 7
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2M2VjNGVhYjNjYmQzYzUwNTczNDY1M2QiLCJpYXQiOjE2NzY0NDAwMzd9.OUZhtiTJX6AZhz46xy3FAi0uwuyaFQ8ik3bQrEHSKP0"
}
## Logout User
- POST {{url}}/users/logout
## Logout All Users
- POST {{url}}/users/logoutAll
## Current User
- GET {{url}}/users/me
- Response Body
{
    "_id": "63ec79233cbd3c50573465b6",
    "name": "AK Santos",
    "email": "aksantos@gmail.com",
    "age": 21,
    "__v": 12
}
## Update User
- PATCH {{url}}/users/me
{
    "age": 21,
    "name": "Ayesha Keith",
    "password": "7777777"
}
- Response Body
{
    "_id": "63ec79233cbd3c50573465b6",
    "name": "Ayesha Keith",
    "email": "aksantos@gmail.com",
    "age": 21,
    "__v": 12
}
## Delete User
- DELETE {{url}}/users/me
- Response Body
{
    "_id": "63ec79233cbd3c50573465b6",
    "name": "Ayesha Keith",
    "email": "aksantos@gmail.com",
    "age": 21,
    "__v": 12
}
## Upload Avatar
- POST {{url}}/users/me/avatar
- body -> form-data -> KEY = avatar -> TYPE = file -> VALUE = avatar.jpg
- Response Body
{
    "_id": "63f2a665c0f7ef2dfca3c049",
    "name": "AK",
    "email": "ayeshakeith@gmail.com",
    "age": 21,
    "createdAt": "2023-02-19T22:44:53.890Z",
    "updatedAt": "2023-02-19T22:45:58.571Z",
    "__v": 5
}
## Delete Avatar
- DELETE {{url}}/users/me/avatar
## View Avatar
- GET {{url}}/users/:id/avatar
- Response Body
## Create Task
- POST {{url}}/tasks
{
    "description": "Complete Electives",
    "completed": true
}
- Response Body
{
    "description": "Complete Electives",
    "completed": true,
    "owner": "63ec7b153cbd3c50573465f6",
    "_id": "63ec7bbb4a4694c9be0942c1",
    "createdAt": "2023-02-15T06:29:15.572Z",
    "updatedAt": "2023-02-15T06:29:15.572Z",
    "__v": 0
}
## Read Tasks
- GET {{url}}/tasks
- Response Body
## Read Task
- GET {{url}}/tasks/:id
- Response Body
{
    "_id": "63ec7bbb4a4694c9be0942c1",
    "description": "Complete Electives",
    "completed": true,
    "owner": "63ec7b153cbd3c50573465f6",
    "createdAt": "2023-02-15T06:29:15.572Z",
    "updatedAt": "2023-02-15T06:29:15.572Z",
    "__v": 0
}
## Update Task
- PATCH {{url}}/tasks/:id
{
    "completed": false
}
- Response Body
{
    "_id": "63ec7bbb4a4694c9be0942c1",
    "description": "Complete Electives",
    "completed": false,
    "owner": "63ec7b153cbd3c50573465f6",
    "createdAt": "2023-02-15T06:29:15.572Z",
    "updatedAt": "2023-02-15T06:33:08.877Z",
    "__v": 0
}
## Delete Task
- DELETE {{url}}/tasks/:id
- Response Body
{
    "_id": "63ec7bbb4a4694c9be0942c1",
    "description": "Complete Electives",
    "completed": false,
    "owner": "63ec7b153cbd3c50573465f6",
    "createdAt": "2023-02-15T06:29:15.572Z",
    "updatedAt": "2023-02-15T06:33:08.877Z",
    "__v": 0
}
## Testing
- npm test