// CRUD operations

const {MongoClient, ObjectID } = require('mongodb')

const connectionURL = 'mongodb://127.0.0.1:27017'
const databaseName = 'task-manager'

MongoClient.connect(connectionURL, { useNewUrlParser: true }, (error, client) => {
    if (error) {
        return console.log('Unable to connect to database!')
    }

    const db = client.db(databaseName)

    // create
    db.collection('tasks').insertMany([
        {
            description: 'Do mock exams',
            completed: true
        }, {
            description: 'Complete electives exercises',
            completed: false
        }, {
            description: 'Take the RHCSA',
            completed: false
        }
    ], (error, result) => {
        if (error) {
            return console.log('Unable to insert tasks!')
        }

        console.log(result.ops)
    })

    // read
    db.collection('tasks').find({ completed: false }).toArray((error, tasks) => {
        console.log(tasks)
    })

    // update
    db.collection('tasks').updateMany({
        completed: false
    }, {
        $set: {
            completed: true
        }
    }).then((result) => {
        console.log(result.modifiedCount)
    }).catch((error) => {
        console.log(error)
    })

    // delete
    db.collection('tasks').deleteOne({
        description: "Complete electives exercises"
    }).then((result) => {
        console.log(result)
    }).catch((error) => {
        console.log(error)
    })

    
})